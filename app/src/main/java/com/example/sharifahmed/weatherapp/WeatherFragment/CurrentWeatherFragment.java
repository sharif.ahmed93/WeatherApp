package com.example.sharifahmed.weatherapp.WeatherFragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.example.sharifahmed.weatherapp.Retrofit.WeatherServiceApi;
import com.example.sharifahmed.weatherapp.Retrofit.BaseUrl;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;
import com.example.sharifahmed.weatherapp.R;
import com.example.sharifahmed.weatherapp.Retrofit.WeatherServiceByCityApi;
import com.example.sharifahmed.weatherapp.SharedPreferenceManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class CurrentWeatherFragment extends Fragment {

    TextView currentLocation;
    TextView updatTime;
    ImageView weatherImage;
    TextView weatherCondtn;
    TextView weatherHumidity;
    TextView currentTemp;
    TextView currentTempUnit;
    TextView currentPressure;
    TextView currentWind;
    TextView weatherIcon;


    TextView tempMin;
    TextView tempMax;
    TextView windSpeed;
    TextView windDeg;
    TextView pressureStat;
    TextView visibility;
    TextView sunrise;
    TextView sunset;

    Context context;

    String current_location;
    String update_time;
    String weather_condition;
    String weather_humidity;
    String current_temp;
    String current_temp_unit;
    AQuery aq;

    SendForecastData SFD;


    WeatherDataResponse weatherDataResponse;
    WeatherServiceApi weatherServiceApi;
    WeatherServiceByCityApi weatherServiceByCityApi;
    SharedPreferenceManager prefs;



    public CurrentWeatherFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefs = SharedPreferenceManager.from(getActivity());
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.current_frag, container, false);

        currentLocation = (TextView) view.findViewById(R.id.CurrentLocationTextId);
        updatTime = (TextView) view.findViewById(R.id.UpdateLocationTimeTextId);
        weatherCondtn = (TextView) view.findViewById(R.id.CurrentWeatherConditonTextId);
        weatherHumidity = (TextView) view.findViewById(R.id.CurrentHumidityTextId);
        currentTemp = (TextView) view.findViewById(R.id.CurrentTempTextId);
        currentPressure = (TextView) view.findViewById(R.id.CurrentPressureTextId);
        currentWind = (TextView) view.findViewById(R.id.CurrentWindTextId);
        currentTempUnit = (TextView) view.findViewById(R.id.TempUnitTextId);
        //weatherImage = (ImageView) view.findViewById(R.id.WeatherImageId);

        tempMin = (TextView) view.findViewById(R.id.tempMin);
        tempMax = (TextView) view.findViewById(R.id.tempMax);
        windSpeed = (TextView) view.findViewById(R.id.windSpeed);
        windDeg = (TextView) view.findViewById(R.id.windDeg);
        pressureStat = (TextView) view.findViewById(R.id.pressureStat);
        visibility = (TextView) view.findViewById(R.id.visibility);
        sunrise = (TextView) view.findViewById(R.id.sunrise);
        sunset = (TextView) view.findViewById(R.id.sunset);

        context = getActivity().getApplicationContext();


        networkLibraryInitialize();
        getWeatherData();

        return view;
    }
    public void networkLibraryInitialize() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //weatherServiceApi = retrofit.create(WeatherServiceApi.class);
        weatherServiceByCityApi = retrofit.create(WeatherServiceByCityApi.class);

    }
    public WeatherDataResponse getWeatherData() {

        Log.i("COUNTRY NAME" , prefs.getCountry());
        Log.i("CITY NAME" , prefs.getCity());
        //Call<WeatherDataResponse> weatherDataResponseCall=weatherServiceApi.getAllWeatherData();
        Call<WeatherDataResponse> weatherDataResponseCall=weatherServiceByCityApi.getAllWeatherDataByCity(getWeatherQueryUrl(prefs.getCountry(),prefs.getCity(),prefs.getUnit()));

        weatherDataResponseCall.enqueue(new Callback<WeatherDataResponse>() {
            @Override
            public void onResponse(Call<WeatherDataResponse> call, Response<WeatherDataResponse> response) {

                if(response.isSuccessful()){
                    WeatherDataResponse weatherData = response.body();
                    WeatherDataResponse.Query dataQuery = weatherData.getQuery();

                    WeatherDataResponse.Results dataResults = dataQuery.getResults();
                    try{
                        WeatherDataResponse.Channel dataChannel = dataResults.getChannel();


                        String w_ChannelTitle = dataChannel.getTitle();
                        String w_ChannelLink = dataChannel.getLink();
                        String w_ChannelDescription = dataChannel.getDescription();
                        String w_ChannelLanguage = dataChannel.getLanguage();
                        String w_ChannelLastBuildDate = dataChannel.getLastBuildDate();
                        String w_ChannelTtl = dataChannel.getTtl();

                        WeatherDataResponse.Astronomy weatherAstronomy = dataChannel.getAstronomy();
                        String w_sunrise = weatherAstronomy.getSunrise();
                        String w_sunset = weatherAstronomy.getSunset();

                        WeatherDataResponse.Atmosphere weatherAtmosphere = dataChannel.getAtmosphere();
                        String w_humidity = weatherAtmosphere.getHumidity();
                        String w_pressure = weatherAtmosphere.getPressure();
                        String w_rising = weatherAtmosphere.getRising();
                        String w_visibility = weatherAtmosphere.getVisibility();

                        WeatherDataResponse.Image weatherImage = dataChannel.getImage();
                        String i_title = weatherImage.getTitle();
                        String i_width = weatherImage.getWidth();
                        String i_height = weatherImage.getHeight();
                        String i_link = weatherImage.getLink();
                        String i_url = weatherImage.getUrl();

                        WeatherDataResponse.Item weatherItem = dataChannel.getItem();
                        String itemTitle = weatherItem.getTitle();
                        String itemLat = weatherItem.getLat();
                        String item_Long = weatherItem.getLong();
                        String itemLink = weatherItem.getLink();
                        String itemPubDate = weatherItem.getPubDate();
                        String itemDescription = weatherItem.getDescription();

                        WeatherDataResponse.Condition weatherCondition = weatherItem.getCondition();
                        String weatherConditionCode = weatherCondition.getCode();
                        String weatherConditionDate = weatherCondition.getDate();
                        String weatherConditionTemp = weatherCondition.getTemp();
                        String weatherConditionText = weatherCondition.getText().toString();



                        ArrayList<WeatherDataResponse.Forecast> weatherForecast=new ArrayList<WeatherDataResponse.Forecast>();
                        weatherForecast = (ArrayList<WeatherDataResponse.Forecast>) weatherItem.getForecast();

                        SFD.sendForecast(weatherForecast);

                        WeatherDataResponse.Guid weatherGuid = weatherItem.getGuid();
                        String isPermaLink = weatherGuid.getIsPermaLink();




                        WeatherDataResponse.Location weatherLocation = dataChannel.getLocation();
                        String weatherLocationCity = weatherLocation.getCity();
                        String weatherLocationCountry = weatherLocation.getCountry();
                        String weatherLocationRegion = weatherLocation.getRegion();

                        WeatherDataResponse.Units weatherUnits = dataChannel.getUnits();
                        String weatherUnitsDistance = weatherUnits.getDistance();
                        String weatherUnitsPressure = weatherUnits.getPressure();
                        String weatherUnitsSpeed = weatherUnits.getSpeed();
                        String weatherUnitsTemperature = weatherUnits.getTemperature();

                        WeatherDataResponse.Wind weatherWind = dataChannel.getWind();
                        String weatherWindChill = weatherWind.getChill();
                        String weatherWindDirection = weatherWind.getDirection();
                        String weatherWindSpeed = weatherWind.getSpeed();




                        //Current_Frag layout Text
                        currentLocation.setText(weatherLocationCity+","+weatherLocationRegion+","+weatherLocationCountry);
                        weatherCondtn.setText(weatherConditionText);
                        currentTemp.setText(weatherConditionTemp);
                        updatTime.setText(w_ChannelLastBuildDate);
                        currentTempUnit.setText(weatherUnitsTemperature);
                        weatherHumidity.setText(w_humidity+"%");
                        currentPressure.setText(w_pressure+" "+weatherUnitsPressure);
                        tempMin.setText(weatherConditionTemp);
                        pressureStat.setText(w_rising);
                        windSpeed.setText(weatherWindSpeed);
                        windDeg.setText(weatherWindDirection);
                        visibility.setText(w_visibility);
                        sunrise.setText(w_sunrise);
                        sunset.setText(w_sunset);



                    }catch (NullPointerException ex){
                        Toast.makeText(getActivity(), "No Data found", Toast.LENGTH_SHORT).show();
                    }


//Current_Weather_Fragment Layout Text
                    /*
                    currentLocation.setText(weatherLocationCity+","+weatherLocationRegion+","+weatherLocationCountry);
                    weatherCondtn.setText(weatherConditionText);
                    currentTemp.setText(weatherConditionTemp);
                    currentTempUnit.setText(weatherUnitsTemperature);
                    weatherHumidity.setText(w_humidity+"%");
                    updatTime.setText(w_ChannelLastBuildDate);
                    //updatTime.setText(weatherForecast.get(2).getDay());
                    currentWind.setText(weatherWindSpeed+" "+weatherUnitsSpeed);
                    currentPressure.setText(w_pressure+" "+weatherUnitsPressure);

                    weatherImage.setHeight(i_height);
                    weatherImage.setWidth(i_width);


                    aq = new AQuery(getActivity());
                    aq.id(R.id.WeatherImageId).image(i_url);
                    */


                }
                else{
                    Log.e("RESPONSE","No Responbse Is Found");
                    Toast.makeText(getActivity(), "No Response", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<WeatherDataResponse> call, Throwable t) {
                Log.e("FAILURE",t.getMessage());
                Toast.makeText(getActivity(), "Failure", Toast.LENGTH_SHORT).show();
            }
        });
        return weatherDataResponse;
    }



    public String getWeatherQueryUrl(String country,String city,String temp){
        String url = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)" +
                "%20where%20text%3D%22"+country+"%2C%20"+city+"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
       // String urlTemp = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+country+"%2C%20"+city+"%22)%20and%2e0u%3d'c'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        String url2 = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20" +
                "(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+country+"%2C%20"+city+"%22)" +
                "%20and%20u%3D'"+temp+"'%20&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

        return url2;
    }


    public interface  SendForecastData{
        public void sendForecast(ArrayList<WeatherDataResponse.Forecast> forecastData);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            SFD = (SendForecastData) activity;
        }catch(ClassCastException e){
            throw  new ClassCastException("You nedd to implement sendForecastData method");
        }

    }


}
