package com.example.sharifahmed.weatherapp.ModelForDataResponse;

import android.content.Context;

import com.example.sharifahmed.weatherapp.SharedPreferenceManager;
import com.example.sharifahmed.weatherapp.WeatherApp;

/**
 * Created by SHARIF AHMED on 5/13/2017.
 */
public class Constants {

    private static Context mContext = WeatherApp.getContext();

    public static final String CITY_NAME = "noakhali";
    public static final String COUNTRY_NAME = "bangladesh";
    public static final String PREV_QUERY = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+ Constants.COUNTRY_NAME+"%2C%20";
    public static final String AFTER_QUERY = "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";


}
