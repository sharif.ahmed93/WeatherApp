package com.example.sharifahmed.weatherapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;
import com.example.sharifahmed.weatherapp.R;

import java.util.ArrayList;

/**
 * Created by SHARIF AHMED on 6/23/2016.
 */
public class CustomFragmentListAdapter extends ArrayAdapter<WeatherDataResponse.Forecast> {

    private Activity activity;
    ArrayList<WeatherDataResponse.Forecast>forecastList;

    public CustomFragmentListAdapter(Activity activity, ArrayList<WeatherDataResponse.Forecast> forecastList) {
        super(activity, R.layout.custom_raw_layout,forecastList);

        this.activity=activity;
        this.forecastList=forecastList;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        convertView = layoutInflater.inflate(R.layout.custom_raw_layout,null);

        TextView dayText = (TextView) convertView.findViewById(R.id.forecastDayId);

        TextView weatherConditionText = (TextView) convertView.findViewById(R.id.forecastWeatherConditionId);
        TextView weatherPresureText = (TextView) convertView.findViewById(R.id.forecastPressureId);
        TextView weatherWindText = (TextView) convertView.findViewById(R.id.forecastWindId);
        TextView weatherTempText = (TextView) convertView.findViewById(R.id.forecastWeatherTempId);

        dayText.setText(forecastList.get(position).getDay());
        weatherConditionText.setText(forecastList.get(position).getText());
        weatherPresureText.setText(forecastList.get(position).getHigh());
        weatherWindText.setText(forecastList.get(position).getLow());
        weatherTempText.setText(forecastList.get(position).getCode());

        return convertView;
    }
}
