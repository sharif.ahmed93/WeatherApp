package com.example.sharifahmed.weatherapp.Retrofit;

import com.example.sharifahmed.weatherapp.ModelForDataResponse.Constants;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by SHARIF AHMED on 5/14/2017.
 */
public interface WeatherServiceByCityApi {
    @GET()      //here is the other url part.best way is to start using /
    //@GET("v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22{'country'}%2C%20{'city'}%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")      //here is the other url part.best way is to start using /
    Call<WeatherDataResponse> getAllWeatherDataByCity(@Url String url);
}
