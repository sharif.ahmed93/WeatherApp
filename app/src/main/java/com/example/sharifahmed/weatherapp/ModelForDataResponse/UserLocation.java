package com.example.sharifahmed.weatherapp.ModelForDataResponse;

/**
 * Created by SHARIF AHMED on 5/23/2017.
 */
public class UserLocation {
    String city;
    String country;

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public UserLocation(String city, String country) {

        this.city = city;
        this.country = country;
    }

}
