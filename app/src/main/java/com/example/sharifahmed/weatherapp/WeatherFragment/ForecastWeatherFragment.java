package com.example.sharifahmed.weatherapp.WeatherFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharifahmed.weatherapp.Adapter.CustomFragmentListAdapter;
import com.example.sharifahmed.weatherapp.Retrofit.WeatherServiceApi;
import com.example.sharifahmed.weatherapp.Retrofit.BaseUrl;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;
import com.example.sharifahmed.weatherapp.R;
import com.example.sharifahmed.weatherapp.Retrofit.WeatherServiceByCityApi;
import com.example.sharifahmed.weatherapp.SharedPreferenceManager;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastWeatherFragment extends Fragment {

    ListView forecastList;
    ArrayList<WeatherDataResponse.Forecast>forecastListItem;

    WeatherDataResponse.Forecast forecast;
    WeatherDataResponse weatherDataResponse;
    WeatherServiceApi weatherServiceApi;
    WeatherServiceByCityApi weatherServiceByCityApi;
    SharedPreferenceManager prefs;

    CustomFragmentListAdapter adapter;
    public ForecastWeatherFragment() {
        // Required empty public constructor
    }

    public  void getForecastData(ArrayList<WeatherDataResponse.Forecast> forecastData){

        forecastListItem = forecastData;
        Log.i("ForcastWeatherFragment",forecastListItem.get(1).getDay()+" "+forecastListItem.get(1).getDate()+" "+forecastListItem.get(1).getText());

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        prefs = SharedPreferenceManager.from(getActivity());
        View view = inflater.inflate(R.layout.forecast_weather_fragment_layout, container, false);
        TextView daytext = (TextView) view.findViewById(R.id.forecastDayId);
        forecastList = (ListView) view.findViewById(R.id.fragmentListViewId);
        networkLibraryInitialize();
        getWeatherData();
        // Inflate the layout for this fragment
        return view;
    }
    public void networkLibraryInitialize() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
       // weatherServiceApi = retrofit.create(WeatherServiceApi.class);
        weatherServiceByCityApi = retrofit.create(WeatherServiceByCityApi.class);
    }
    public WeatherDataResponse getWeatherData() {

        //Call<WeatherDataResponse> weatherDataResponseCall=weatherServiceApi.getAllWeatherData();
        Call<WeatherDataResponse> weatherDataResponseCall=weatherServiceByCityApi.getAllWeatherDataByCity(getWeatherQueryUrl(prefs.getCountry(),prefs.getCity(),prefs.getUnit()));
        weatherDataResponseCall.enqueue(new Callback<WeatherDataResponse>() {
            @Override
            public void onResponse(Call<WeatherDataResponse> call, Response<WeatherDataResponse> response) {

                WeatherDataResponse weatherData = response.body();
                ArrayList<WeatherDataResponse.Forecast> weatherForecast=new ArrayList<WeatherDataResponse.Forecast>();
                try {
                    weatherForecast = (ArrayList<WeatherDataResponse.Forecast>) weatherData.getQuery().getResults().getChannel().getItem().getForecast();
                    adapter = new CustomFragmentListAdapter(getActivity(), (ArrayList<WeatherDataResponse.Forecast>) weatherForecast);
                    forecastList.setAdapter(adapter);
                }catch (NullPointerException ex){
                    Toast.makeText(getActivity(), "No Data Found", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<WeatherDataResponse> call, Throwable t) {

            }
        });
        return weatherDataResponse;
    }

    public String getWeatherQueryUrl(String country,String city,String temp){
        String url = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20" +
                "(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+country+"%2C%20"+city+"%22)" +
                "&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

        String url2 = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20" +
                "(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+country+"%2C%20"+city+"%22)" +
                "%20and%20u%3D'"+temp+"'%20&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=";

       // String urlTemp = "v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+country+"%2C%20"+city+"%22)%20and%2e0u%3d'"+temp+"'&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        return url2;
    }

}
