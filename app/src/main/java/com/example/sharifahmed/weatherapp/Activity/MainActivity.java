package com.example.sharifahmed.weatherapp.Activity;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.example.sharifahmed.weatherapp.Adapter.FragmentPageAdapter;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.UserLocation;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;
import com.example.sharifahmed.weatherapp.R;
import com.example.sharifahmed.weatherapp.SharedPreferenceManager;
import com.example.sharifahmed.weatherapp.WeatherFragment.CurrentWeatherFragment;
import com.example.sharifahmed.weatherapp.WeatherFragment.ForecastWeatherFragment;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainActivity extends FragmentActivity implements CurrentWeatherFragment.SendForecastData,
        ActionBar.TabListener {

    ActionBar actionBar;
    ViewPager viewPager;
    FragmentPageAdapter pagerAdapter;

    CurrentWeatherFragment currentWeatherFragment;

    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager= (ViewPager) findViewById(R.id.mainViewPagerId);

        pagerAdapter = new FragmentPageAdapter(getSupportFragmentManager());
        actionBar=getActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.rgb(4,154,192)));
        viewPager.setAdapter(pagerAdapter);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        actionBar.addTab(actionBar.newTab().setText("Current").setTabListener(this));
        actionBar.addTab(actionBar.newTab().setText("Forecast").setTabListener(this));


    }
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }


    @Override
    public void sendForecast(ArrayList<WeatherDataResponse.Forecast> forecast) {


        ForecastWeatherFragment weatherFragment = new ForecastWeatherFragment();
       // ForecastWeatherFragment forecastWeatherFragment =  (ForecastWeatherFragment) getSupportFragmentManager().findFragmentById(R.id.forecast);
        weatherFragment.getForecastData(forecast);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu,menu);
        return true;
    }

    public void goSettingsActivity(MenuItem menu){

        //Toast.makeText(this,"Clicked Home",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(),SettingsActivity.class);
        startActivity(intent);

    }

    public void goRefresh(MenuItem item) {
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        startActivity(intent);
    }



}
