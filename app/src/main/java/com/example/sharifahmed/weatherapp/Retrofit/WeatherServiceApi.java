package com.example.sharifahmed.weatherapp.Retrofit;

import com.example.sharifahmed.weatherapp.ModelForDataResponse.Constants;
import com.example.sharifahmed.weatherapp.ModelForDataResponse.WeatherDataResponse;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by SHARIF AHMED on 8/3/2016.
 */
public interface WeatherServiceApi {

    @GET("v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+ Constants.COUNTRY_NAME+"%2C%20"+Constants.CITY_NAME+"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys")      //here is the other url part.best way is to start using /
    Call<WeatherDataResponse> getAllWeatherData();
}
