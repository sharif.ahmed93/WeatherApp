package com.example.sharifahmed.weatherapp.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Switch;

import com.example.sharifahmed.weatherapp.WeatherFragment.CurrentWeatherFragment;
import com.example.sharifahmed.weatherapp.WeatherFragment.ForecastWeatherFragment;

/**
 * Created by SHARIF AHMED on 8/3/2016.
 */
public class FragmentPageAdapter extends FragmentPagerAdapter {
    public FragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {


        switch (position){
            case 0 :
                return  new CurrentWeatherFragment();
            case 1:
                return  new ForecastWeatherFragment();
            default:
                break;
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
