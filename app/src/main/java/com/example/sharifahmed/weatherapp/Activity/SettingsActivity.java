package com.example.sharifahmed.weatherapp.Activity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.sharifahmed.weatherapp.R;
import com.example.sharifahmed.weatherapp.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    LinearLayout linearCityId;
    LinearLayout linearCountryId;
    LinearLayout linearTempId;
    LinearLayout linearPressureId;
    int id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
/*
        Fragment fragment = new SettingsScreen();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        if(savedInstanceState == null){
            fragmentTransaction.add(R.id.settingPreferenceFragmentid,fragment,"setting_fragment");
            fragmentTransaction.commit();
        }
        else{
            fragment = getFragmentManager().findFragmentByTag("setting_fragment");
        }
*/


        getFragmentManager().beginTransaction()
                .replace(R.id.settingPreferenceFragmentid, new SettingsFragment()).commit();



    }


    /*
    public static class SettingsFragment extends PreferenceFragment {

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            addPreferencesFromResource(R.xml.preferences);
            SwitchPreference switchPreference = (SwitchPreference) findPreference("switch");

            switchPreference.setEnabled(true);

        }
    }
*/

}
